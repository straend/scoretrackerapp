package com.example.highscorer;


import java.util.Observable;
import java.util.Observer;

import com.example.highscorer.Responses.RegisteredPlayersResponse;
import com.example.highscorer.Responses.Response;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity implements Observer {
	private RestClient r;
	private boolean gotoNewGame		= false;
	private Intent i;
	private ProgressDialog pd;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        this.r = new RestClient(this);
        this.r.addObserver(this);
        
    }
    
    private void showDialog(String title, String message){
    	this.pd = new ProgressDialog(this);
    	this.pd.setTitle(title);
    	this.pd.setMessage(message);
    	this.pd.show();
    	 
    }
    
    public void viewPlayers(View view){
    	showDialog("Dont worry", "I am getting Your data");
    	gotoNewGame=false;
    	
    	if (r.getPlayers()){
    		Log.d("AS", "Getting players");
    	} else {
    		Log.d("AS", "Busy");
    	}
    	
    	
    }
    public void getReg(View view){
    	showDialog("Dont worry", "I am getting Your data");
    	gotoNewGame=false;
    	
    	if (r.getRegisteredNames()){
    		Log.d("AS", "Getting reg");
    	} else {
    		Log.d("AS", "Busy");
    	}
    	
    }
    public void newGame(View view){
    	showDialog("Dont worry", "I am getting Your data");
    	gotoNewGame=true;
    	
    	i = new Intent(this, NewGameActivity.class);
    	
    	r.getPlayers();
    	
    }
    private void newGamePlayers(String players){
    	i.putExtra("players", players);
    	r.getRegisteredNames();
    }
    private void newGameRegistered(String registered){
    	gotoNewGame= false;
    	
    	this.pd.dismiss();
    	i.putExtra("registered", registered);
    	this.startActivity(this.i);
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.fik1_link:
            	Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://arcadabandy.fik1.net/media/fik1_rootcert.pem"));
            	startActivity(browserIntent);
                return true;
            case R.id.menu_settings:
            	Intent i = new Intent(this, SettingsActivity.class);
        		this.startActivity(i);
        		return true;
        	default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    
    private void playerListResponse(String result){
    	this.pd.dismiss();
    	
    	Intent i = new Intent(this, ShowPointsActivity.class);
		i.putExtra("playersJSON", result);
		
    	this.startActivity(i);
    }
    private void regPlayers(String result){
    	this.pd.dismiss();
    	final RegisteredPlayersResponse r = new RegisteredPlayersResponse(result);
    	
    	runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(getApplicationContext(), r.toString() , Toast.LENGTH_LONG).show();
				
			}
		});
    }
    
    
	public void update(Observable arg0, Object arg1) {
		if(arg0.getClass() != RestClient.class){
			return;
		}
		final Response res = (Response) arg1;
		
		switch (res.type) {
		
		case PLAYERS_LIST:
			if(gotoNewGame){
				newGamePlayers(res.response);
			} else {
				playerListResponse(res.response);
			}
			break;
			
		case REGISTERED_FOR_GAME:
			if(gotoNewGame){
				newGameRegistered(res.response);
			} else {
				regPlayers(res.response);
			}
			break;
			
		case ERROR:
			this.pd.dismiss();
			gotoNewGame = false;
			runOnUiThread(new Runnable() {
				public void run() {
					Toast.makeText(getApplicationContext(), "Something went wrong\n"+res.getData(), Toast.LENGTH_LONG).show();
				}
			});
			break;
			
		default:
			break;
		}
		
		
	}

}
