package com.example.highscorer;

import org.json.JSONException;
import org.json.JSONObject;

public class Player {
	private String name;
	
	private int goals, assists, games, points;
	private int id;
	private float ratio;
	
	public Player(JSONObject json){
		try {
			this.name 		= json.getString("name");
			this.games		= json.getInt("games");
			this.goals 		= json.getInt("goals");
			this.assists 	= json.getInt("assists");
			this.id 		= json.getInt("id");
			
		
			updateRatioPoints();
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	public Player(String name, int goals, int assists, int games){
		this.name = name;
		this.goals = goals;
		this.assists = assists;
		this.games = games;
		
		updateRatioPoints();
	}
	public Player(String name){
		this.name = name;
		this.goals = 0;
		this.assists = 0;
		this.games = 0;
		
		updateRatioPoints();
	}
	public JSONObject getJSON(){
		JSONObject o = new JSONObject();
		try {
			o.put("name", this.name);
			o.put("goals", this.goals);
			o.put("games", this.games);
			o.put("assists", this.assists);
			o.put("id", this.id);	
			
			o.put("Ratio", this.ratio);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
		return o;
	}
	
	private void updateRatioPoints(){
		this.points = this.goals + this.assists;
		
		if(this.points!=0 && this.games != 0)
			this.ratio = this.points/this.games;
		else
			this.ratio = 0;
	}
	public String toString(){
		return this.name + " Games: " + this.games +" Points: "+this.points+" Ratio: "+this.ratio;
	}
	public String getName() {
		return this.name;
	}

	public String getRatio() {
		return Float.toString(this.ratio);
	}
	
	public String getGames() {
		return Integer.toString(this.games);
	}
	
	public String getGoals() {
		return Integer.toString(this.goals);
	}
	
	public void addGoal() {
		this.goals++;
		updateRatioPoints();
		
	}
	public void removeGoal() {
		if(this.goals>0){
			this.goals--;
			updateRatioPoints();
		}
	}
	
	public String getAssists() {
		return Integer.toString(this.assists);
	}

	public void addAssist() {
		this.assists++;
		updateRatioPoints();
		
	}

	public void removeAssist() {
		if (this.assists>0){
			this.assists--;
			updateRatioPoints();
		}
		
	}

	public void setGoals(int goals) {
		this.goals=goals;
		updateRatioPoints();
	}

	public void setAssists(int assists) {
		this.assists = assists;
		updateRatioPoints();
		
	}
}
