package com.example.highscorer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Observable;

import org.json.JSONArray;

import com.example.highscorer.Responses.ErrorResponse;
import com.example.highscorer.Responses.Response;
import com.example.highscorer.Responses.Response.ResponseType;
import com.example.highscorer.Responses.PlayerListResponse;
import com.example.highscorer.Responses.PostGameDataResponse;
import com.example.highscorer.Responses.RegisteredPlayersResponse;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;


public class RestClient extends Observable implements Runnable {
	private String baseUrl = "https://arcadabandy.fik1.net/api/v1/";
	Boolean isRunning = false;
	private String targetUrl;
	private ResponseType responseType;
	
	private String username;
	private String password;
	private String auth;
	
	private String data;
	private String method;
	
	
	public RestClient(final String username, final String password){
		this.username = username;
		this.password = password;
		this.auth = Base64.encodeToString(
				(this.username+":"+this.password).getBytes(),
				Base64.DEFAULT).trim();
	}
	public RestClient(Context c){
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(c);
		String user = sharedPref.getString(SettingsActivity.KEY_PREF_USERNAME, "");
		String pass = sharedPref.getString(SettingsActivity.KEY_PREF_PASSWORD, "");
		Log.d("REST", user+":"+pass);
		this.username = user;
		this.password = pass;
		this.auth = Base64.encodeToString(
				(this.username+":"+this.password).getBytes(),
				Base64.DEFAULT).trim();
		
	}
	public boolean getPlayers(){
		if (this.isRunning)
			return false;
		this.data = null;
		this.method = "GET";
		
		this.responseType = ResponseType.PLAYERS_LIST;
		this.targetUrl = this.baseUrl+"players/";
		new Thread(this).start();
		
		return true;
	}
	
	public boolean getRegisteredNames(){
		if (this.isRunning)
			return false;
		
		this.data = null;
		this.method = "GET";
		this.responseType = ResponseType.REGISTERED_FOR_GAME;
		this.targetUrl = "http://people.arcada.fi/~karlssoj/players.html";
		new Thread(this).start();
		
		return true;
	}
	public boolean postNewGame(ArrayList<Player> players) {
		if (this.isRunning)
			return false;
		
		JSONArray arr = new JSONArray();
	
		this.data = "[";
		for (Player player : players) {
			//this.data+="{"+player.toJ()+"},";
			arr.put(player.getJSON());
		}
		this.data = this.data.substring(0, this.data.length()-1);
		this.data += "]";
		
		this.data = arr.toString();
		this.method = "PUT";
		this.responseType = ResponseType.POST_GAME_RESPONSE;
		this.targetUrl = baseUrl+"players/";
		
		new Thread(this).start();
		
		return true;
		
	}
	
	public void run() {
		this.isRunning = true;		
		URL url;
    	HttpURLConnection uc = null;
		String res="";
		Response response = null;
		
		try {
			url = new URL(this.targetUrl);
			uc = (HttpURLConnection) url.openConnection();
			uc.addRequestProperty("Authorization", "Basic "+this.auth);
			uc.setRequestProperty("Accept", "application/json");
			uc.setRequestProperty("Content-Type", "application/json");
			uc.setRequestMethod(this.method);
			
			//Post/Put the data
			if(this.data != null && (this.method == "POST" || this.method == "PUT")){
				OutputStreamWriter ow = new OutputStreamWriter(uc.getOutputStream());
				ow.write(this.data);
				ow.close();
			}
			
			res = getStringFromStream(uc.getInputStream());

			// Create Response from reslut
			switch (this.responseType) {
			case PLAYERS_LIST:
				response = new PlayerListResponse(res);
				break;
			case REGISTERED_FOR_GAME:
				response = new RegisteredPlayersResponse(res);
				break;
			case POST_GAME_RESPONSE:
				response = new PostGameDataResponse(res);
				Log.d("ASD", "GOT RESPONSE: "+res);
				break;
			default:
				response = new ErrorResponse("No Responsetype found\n"+res);
				break;
			}
		} catch (MalformedURLException e) {
			response = new ErrorResponse(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			response = new ErrorResponse(e.getMessage());
			e.printStackTrace();
		} finally{
			uc.disconnect();
			
		}
		
		this.isRunning = false;
		setChanged();
		notifyObservers(response);
			
			
	}
	
	private String getStringFromStream(InputStream in){
		StringBuilder b = new StringBuilder();
		BufferedReader f = new BufferedReader(new InputStreamReader(in));
		String line = null;
		
		try {
			while( (line=f.readLine()) != null)
				b.append(line + "\n");

		} catch (IOException e) {
			e.printStackTrace();

		} finally{
			try {
				in.close();
				f.close();
			} catch (IOException e) {

				e.printStackTrace();
			}
			
		}
		return b.toString();
		
	}



	
	
}
