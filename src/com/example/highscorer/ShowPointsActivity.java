package com.example.highscorer;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;


import com.example.highscorer.Responses.PlayerListResponse;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ListView;

import android.widget.Toast;


public class ShowPointsActivity extends Activity implements Observer{
	
	private ArrayList<Player> playerList = null;
    private ListView playersListView= null;
     
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.activity_showpoints);
        
        PlayerListResponse pResp = new PlayerListResponse(
        		getIntent().getStringExtra("playersJSON")
        );

        playerList = pResp.getPlayers();
    	

	     
	    playersListView = (ListView)findViewById(R.id.listComment);
	
	    PlayersAdapter adapter = new PlayersAdapter(this, R.id.txtLoadingList, playerList);
	    playersListView.setAdapter(adapter);
	     
	    //Manage the onItemClick method
	    playersListView.setOnItemSelectedListener(new OnItemSelectedListener() {
	 
	        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
	            Player player = (Player)parent.getItemAtPosition(position);
	            int duration = Toast.LENGTH_SHORT;
	            Toast toast = Toast.makeText(ShowPointsActivity.this,
	                    "You clicked on a comment by "+player.getName(), duration);
	            toast.show();
			}
	
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
	    });
	    
	    
    }

	public void update(Observable observable, Object data) {
		
		
	}
     
}
