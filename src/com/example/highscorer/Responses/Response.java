package com.example.highscorer.Responses;

import org.json.JSONArray;



public class Response{
	
	

	public static enum ResponseType{
		PLAYERS_LIST,
		GAMES_LIST,
		CREATE_PLAYER,
		UPDATE_PLAYER,
		REGISTERED_FOR_GAME,
		POST_GAME_RESPONSE,
		ERROR,
		NO_RESPONSE
	}
	
	public final ResponseType type;
	public final JSONArray data;
	public final String response;
	
	public Response(ResponseType type, JSONArray jsobj) {
		this.type = type;
		this.data = jsobj;
		this.response = null;
		
	}
	
	public Response(ResponseType type, String response){
		this.type = type;
		this.data = null;
		this.response = response;
	}
	
	public String getData(){
		if (this.data != null)
			return this.data.toString();
		else if (this.response != null)
			return this.response;
		
		
		return "";
	}
	
}
