package com.example.highscorer.Responses;

import org.json.JSONException;
import org.json.JSONObject;


public class PostGameDataResponse extends Response{
	private int updatedPlayers;
	private int requestedAmount;
	private int createdPlayers;
	
	public PostGameDataResponse(String response){
		super(ResponseType.POST_GAME_RESPONSE, response);
		
		try {
			JSONObject jsobj = new JSONObject(response);
			
			this.updatedPlayers = jsobj.getInt("updated");
			this.createdPlayers = jsobj.getInt("created");
			this.requestedAmount = jsobj.getInt("request");
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public int getUpdatedPlayers(){
		return this.updatedPlayers;
	}
	public int getCreatedPlayers(){
		return this.createdPlayers;
	}
	public int getRequestedChanges(){
		return this.requestedAmount;
	}
	
}