package com.example.highscorer.Responses;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import com.example.highscorer.Player;


public class PlayerListResponse extends Response{
	private ArrayList<Player> players;
		
	
	public PlayerListResponse(String response){
		super(ResponseType.PLAYERS_LIST, response);
		this.players = new ArrayList<Player>();
		try {
			JSONArray pl = new JSONArray(response);
			for(int i=0; i< pl.length(); i++){
				Player p;
				p = new Player(pl.getJSONObject(i));
				this.players.add(p);
			}
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
	}
	
	public ArrayList<Player> getPlayers(){
		return this.players;
	}

}

