package com.example.highscorer.Responses;

import java.util.ArrayList;

import com.example.highscorer.Player;


public class RegisteredPlayersResponse extends Response {

	private String[] registeredPlayers;
	
	public RegisteredPlayersResponse(ResponseType type, String response) {
		super(type, response);
		this.registeredPlayers = this.response.split("\n");
		
	}
	
	public RegisteredPlayersResponse(String response){
		super(ResponseType.REGISTERED_FOR_GAME, response);
		
		this.registeredPlayers = this.response.split("\n");
		
	}
	public String[] getRegisteredPlayersString(){
		return this.registeredPlayers;
	}
	
	public ArrayList<Player> getRegisteredPlayers(){
		ArrayList<Player> players = new ArrayList<Player>(registeredPlayers.length);
		for (String p : registeredPlayers) {
			players.add(new Player(p, 0, 0, 0));
		}
		
		return players;
	}
	public String toString(){
		String res="";
		for (String s : registeredPlayers) {
			res+=s+"\n\r";
		}
		return res;
	}
}
