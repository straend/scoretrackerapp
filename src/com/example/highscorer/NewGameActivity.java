package com.example.highscorer;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;


import com.example.highscorer.Responses.PlayerListResponse;
import com.example.highscorer.Responses.PostGameDataResponse;
import com.example.highscorer.Responses.RegisteredPlayersResponse;
import com.example.highscorer.Responses.Response;

import android.app.Activity;
import android.app.ActionBar.LayoutParams;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;



public class NewGameActivity extends Activity implements OnClickListener, OnLongClickListener, OnItemSelectedListener, Observer {
	// These variables are used to distinguish what kind of button has been pressed,
	// And for ID'ing TextViews associated with players
	private static final int button_goal 	= 0x200;
	private static final int button_assist 	= 0x500;
	private static final int textViewid 	= 0x900;
	
	
	private ArrayList<Player> players;
	Spinner sp;
	ArrayAdapter<String> adapter;
	ArrayList<Player> notRegistered;
	RestClient r;
	private ProgressDialog pd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        
		setContentView(R.layout.activity_new_game);
	
        // Create a RestClient with user:pass from Shared settings
		this.r = new RestClient(this);
        this.r.addObserver(this);
        
        // Get Registered players, and players in database
		PlayerListResponse pResp = new PlayerListResponse(
        		getIntent().getStringExtra("players")
        );
        RegisteredPlayersResponse rResp = new RegisteredPlayersResponse(
        		getIntent().getStringExtra("registered")
        );
        
        players = rResp.getRegisteredPlayers();
        notRegistered = pResp.getPlayers();
		
        ArrayList<Player> registered = rResp.getRegisteredPlayers();
		
        // Add player's Spinner
        ArrayList<String> spinnerArray = new ArrayList<String>();
		spinnerArray.add(new String("Add player"));
		sp = (Spinner) findViewById(R.id.playersSpinner);

		
		//Remove registered players from players from database
		for(Player p : registered){
			for (Player pn : notRegistered){
				if (pn.getName().compareToIgnoreCase(p.getName())==0){
					notRegistered.remove(pn);
					break;
				}
			}
		}

		// Add non registered players to spinner
		for (Player player : notRegistered) {
			spinnerArray.add(player.getName());
			
		}
		
		// Create the spinner
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
		sp.setAdapter(adapter);
		sp.setOnItemSelectedListener(this);
		sp.setSelection(0);
		
		
		// Add registered players to list
        int i=0;
        for (Player p : players) {
        	addPlayerToList(i++, p);
		}
        
	}
	
	public void addPlayerToList(int i, Player p) {
		TableLayout tl = (TableLayout) findViewById(R.id.table_players);

		// Create a new row to be added. 
		TableRow tr = new TableRow(this);
		TextView t = new TextView(this);
		t.setId(i+textViewid);
		t.setText(p.getName() + "("
				+ p.getGoals() + "+"
				+ p.getAssists() + ")");
		tr.addView(t);

		// Knappen för att sätta till mål 
		Button bGoal = new Button(this);
		bGoal.setText("add");
		bGoal.setId(button_goal + i);
		
		// Add Button to row.
		bGoal.setLongClickable(true);
		bGoal.setOnClickListener(this);
		bGoal.setOnLongClickListener(this);
		tr.addView(bGoal);
		
		// Knappen för att sätta till passning 
		Button bPass = new Button(this);
		bPass.setText("add");
		bPass.setId(button_assist + i);
		
		
		bPass.setLongClickable(true);
		bPass.setOnClickListener(this);
		bPass.setOnLongClickListener(this);
		// Add Button to row. 
		tr.addView(bPass);

		// Add row to TableLayout. 
		tl.addView(tr, new TableLayout.LayoutParams(
				LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT)
		);
	}
	
	public void onClick(View view){
		int id = view.getId();
		
		Player p = null;
		// Check which button has been pressed, compare with bitwise AND and id of button 
		if( (id & button_goal)>0 ){
			id = id - button_goal;
			
			p = players.get(id);
			p.addGoal();
			((Button) view).setText(""+p.getGoals());
		
		} else if( (id & button_assist)>0 ){
			id = id - button_assist;
			p = players.get(id);
			p.addAssist();
			((Button) view).setText(""+p.getAssists());
		} 
		
		TextView t = (TextView) findViewById(id+textViewid);
		t.setText(p.getName() + "(" 
				+ p.getGoals() + "+"
				+ p.getAssists() + ")"
		);
		
	}
	public boolean onLongClick(View view) {
		int id = view.getId();
		
		Player p = null;
		// Check which button has been pressed, compare with bitwise AND and id of button
		if( (id & button_goal)>0 ){
			id = id - button_goal;
			p = players.get(id);
			p.removeGoal();
			((Button) view).setText(""+p.getGoals());
		
		} else if( (id & button_assist)>0 ){
			id = id - button_assist;
			p = players.get(id);
			p.removeAssist();
			((Button) view).setText(""+p.getAssists());
		} 
		
		TextView t = (TextView) findViewById(id+textViewid);
		t.setText(p.getName() + "(" 
				+ p.getGoals() + "+"
				+ p.getAssists() + ")"
		);
		
		return true; // Taken care of the click
	}
	
	public void saveButtonClick(View view){
		showDialog("Dont worry", "I am posting Your player data");
		r.postNewGame(players);
		
	}
    private void showDialog(String title, String message){
    	this.pd = new ProgressDialog(this);
    	this.pd.setTitle(title);
    	this.pd.setMessage(message);
    	this.pd.show();
    	 
    }
    
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		
		if (arg2 != 0) {

			Player p = new Player(notRegistered.get(arg2-1).getName());
			
			players.add(p);
			addPlayerToList(players.size()-1, p);

			sp.setSelection(0);
			notRegistered.remove(arg2-1);
			adapter.remove(adapter.getItem(arg2));
			adapter.notifyDataSetChanged();
		}
	}
	
	public void gotPostResult(final PostGameDataResponse response){
		final Context cont = this.getApplicationContext();
		
		// Show a nice Toast with information of PostResult
		runOnUiThread(new Runnable() {
			
			public void run() {
				Toast.makeText(cont,
					"Created "+response.getCreatedPlayers() +" Players \n"+
					"Updated "+response.getUpdatedPlayers() +" Players \n"+
					(response.getCreatedPlayers()+response.getUpdatedPlayers()) +" of " + response.getRequestedChanges() + " Updated"
					, Toast.LENGTH_LONG).show();				
			}
		});
		
		// Finnish this activity and go back to mainactivity
		finish();
		
	}
	
	public void onNothingSelected(AdapterView<?> arg0) {
		
	}
	
	public void update(Observable observable, Object data) {
		if(observable.getClass() != RestClient.class){
			return;
		}
		this.pd.dismiss();
		
		final Response res = (Response) data;
		
		if(null == res.type)
			return;
		
		switch (res.type) {
		case POST_GAME_RESPONSE:
			gotPostResult(new PostGameDataResponse(res.response));
			break;
		default:
			break;
		}
		
	}

}
