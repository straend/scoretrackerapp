package com.example.highscorer;

import java.util.ArrayList;
import java.util.List;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class PlayersAdapter extends ArrayAdapter<Player> {

	private Context m_context = null;
	private ArrayList<Player> m_players = null;
	private LayoutInflater m_inflater = null;
	
	public PlayersAdapter(Context context, int textViewResourceId,
			List<Player> objects) {
		super(context, textViewResourceId, objects);
		
		m_context = context;
		m_players = (ArrayList<Player>)objects;
		m_inflater = LayoutInflater.from(m_context);
	}

	@Override
	public int getCount() {
		if( m_players != null ){
			return m_players.size();
		}
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		PlayerViewHolder holder = null;
		
		//form the view
		if( convertView == null ){
			holder = new PlayerViewHolder();
			
			convertView = m_inflater.inflate(R.layout.points_view_item, parent, false);
			

			holder.name 	= (TextView) convertView.findViewById(R.id.v_name);
			holder.goals 	= (TextView) convertView.findViewById(R.id.v_goals);
			holder.assists 	= (TextView) convertView.findViewById(R.id.v_assists);
			holder.games 	= (TextView) convertView.findViewById(R.id.v_games);
			holder.ratio 	= (TextView) convertView.findViewById(R.id.v_ratio);
			
			
			convertView.setTag(holder);
			
		}else{
			
			holder = (PlayerViewHolder)convertView.getTag();
		}
		
		//bind the data to the view
		Player player = m_players.get(position);

		holder.name.setText(	player.getName());
		holder.goals.setText(	player.getGoals());
		holder.assists.setText(	player.getAssists());
		holder.ratio.setText(	player.getRatio());
		holder.games.setText(	player.getGames());

		
		return convertView;
	}

	private class PlayerViewHolder{
		TextView name;
		TextView goals;
		TextView assists;
		TextView games;
		TextView ratio;
	}
	
}
