package com.example.highscorer;

import android.app.Activity;
import android.os.Bundle;


public class SettingsActivity extends Activity {
    public static final String KEY_PREF_USERNAME = "pref_username";
    public static final String KEY_PREF_PASSWORD = "pref_password";

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
    
   

}
